<?php

/**
 * FIeld handler to render a Flip link.
 */
class flip_handler_field_link extends views_handler_field {
  protected $flip_type;
  protected $flip_info;

  function construct() {
    parent::construct();
    $this->flip_type = $this->definition['flip_type'];
    $this->flip_info = $this->definition['flip_info'];

    $this->additional_fields[$this->flip_info['id_key']] = $this->flip_info['id_key'];
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function access() {
    // @todo implement access info in hook_flip_info()
    // return user_access('access world');
    return TRUE;
  }

  function render($values) {
    $build = array(
      '#type' => 'flip',
      '#flip_type' => $this->flip_type,
      '#id_value' => $values->{$this->flip_info['id_key']},
    );
    return drupal_render($build);
  }
}
