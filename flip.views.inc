<?php

/**
 * Implements hook_views_data().
 */
function flip_views_data_alter(&$data) {
  foreach (flip_info() as $name => $info) {
    $data[$info['base_table']]['flip_' . $name] = array(
      'field' => array(
        'title' => 'Flip: ' . $info['title'],
        'help' => $info['description'],
        'handler' => 'flip_handler_field_link',
        'flip_type' => $name,
        'flip_info' => $info,
      ),
    );
  }
}
