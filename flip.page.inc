<?php

/**
 * Page callback that does the actual flipping and returns the appropriate
 * response.
 */
function flip_ajax_callback($type = 'nojs') {
  $data = $_REQUEST['flip_data'];
  $element = $_REQUEST['flip_rebuild_info'];
  $id = $data['id'];
  $record = array(
    $data['id_key'] => $data['id_value'],
    $data['target_key'] => $data['target_value'],
  );

  $return = drupal_write_record($data['base_table'], $record, $data['id_key']);

  if ($type == 'ajax') {
    if ($return == SAVED_UPDATED) {
      $commands[] = ajax_command_replace($id, drupal_render($element));
    }
    else {
      // @todo watchdog opportunity: Print arguments sent to drupal_write_record.
      $commands[] = ajax_command_alert('Sorry! An update could not be made!');
    }

    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
  else {
    if ($return == SAVED_UPDATED) {
      // @todo write a more meaningful message, or something.
      drupal_set_message('Looks like value was updated, thank you!');
      drupal_goto($_REQUEST['flip_destination']['destination']);
    }
    else {
      // @todo watchdog opportunity: Print arguments sent to drupal_write_record.
      drupal_set_message('Do\'h! No update was made.');
    }
  }
}
