<?php

/**
 * Allow modules to declare flip types to easily reference in a render element.
 *
 * @return
 *   An associative array describing the flip types critical default
 *   information. The array contains a sub-array for each flip type, with the
 *   machine-readable name as the key. Each sub-array should have these items:
 *    - "title": A useful title, usually only applicable in user-interfaces,
 *      like Views.
 *    - "description": Think of it more like a tweet. Nobody will read it
 *      anyway.
 *    - "base_key": The database table to lookup.
 *    - "id_key": Usually the primary key, but any db field name to query
 *      against.
 *    - "target_key": the db field name that will be modified.
 *    - "on_label": The text displayed on the link for its current "On" state.
 *    - "off_label": The text displayed on the link for its current "Off"
 *      state.
 *
 *   Any additional items will be injected into the render element as a
 *   property. This is an example of how you would actually use the flip type
 *   in code. Eventually you would have to drupal_render($build) since it is a
 *   renderable array, but Drupal usually takes care of that for you.
 *
 * @code
 *  $build['node_sticky_toggle_link'] = array(
 *    '#type' => 'flip',
 *    '#flip_type' => 'node_sticky',
 *    '#id_value' => 2, // Usually this will be dynamic
 *  );
 * @endcode
 */
function hook_flip_info() {
  return array(
    'user' => array(
      'title' => t('User'),
      'description' => t('Toggle whether a user is blocked, or not, whatev.'),
      'base_table' => 'users',
      'id_key' => 'uid',
      'target_key' => 'status',
      'on_label' => t('Active'),
      'off_label' => t('Blocked'),
    ),
    'node_sticky' => array(
      'title' => t('Node Sticky'),
      'description' => t('Toggle if the node is "sticky".'),
      'base_table' => 'node',
      'id_key' => 'nid',
      'target_key' => 'sticky',
      'on_label' => t('Sticky'),
      'off_label' => t('Not Sticky'),
    ),
  );
}

/**
 * Opportunity to really mess things up for other modules.
 */
function hook_flip_info_alter(&$info) {
  $info['user']['off_label'] = t('Banned');
}
